#ifndef __DICO__
#define __DICO__

#include <stdbool.h>

#include "syntabs.h"

#define maxDico 100
#define INTEGER_SIZE (4)

#define NO_COMPLEMENT -1
#define NO_CLASSE 0

#define C_VARIABLE_GLOBALE 1
#define C_FONCTION 1 // same as variable global
#define C_VARIABLE_LOCALE 2
#define C_ARGUMENT 3

#define T_ENTIER 1
#define T_TABLEAU_ENTIER 2
#define T_FONCTION 3

typedef struct {
	char *identif;
	int classe; // portée : cf. defines
	int type;
	int adresse;
	int complement; // taille d'un tableau, nombre d'arguments d'une fonction
} desc_identif;

typedef struct {
	desc_identif tab[maxDico];
	int base;
	int sommet;
} dico_;

extern char *strdup(const char *s); // To avoid warning:
// implicit declaration of function ‘strdup’ [-Wimplicit-function-declaration]

void entreeFonction(void);
void sortieFonction(void);
int ajouteIdentificateur(char *identif,  int classe, int type, int adresse, int complement);
int rechercheExecutable(char *identif);
int rechercheDeclarative(char *identif);
bool function_in_symbol_table(char *function_name);
bool var_in_symbol_table(char *var_name);
bool var_not_global(char *var_name);
bool var_is_local_or_arg(char *var_name);
bool var_is_local(char *var_name);
bool var_is_global(char *var_name);
bool var_is_declared(char *var_name);
bool var_is_tableau(char *var_name);
int get_var_address(char *var_name);

void affiche_dico(void);
int calculate_address(int contexte, int size_new_var);

dico_ dico;
extern int contexte;
extern int adresseLocaleCourante;
extern int adresseArgumentCourant;
extern int adresseGlobaleCourante;

#endif
