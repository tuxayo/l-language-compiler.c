#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "analyseur_syntaxique.h"

extern FILE *yyin;

extern int token;

int main(int argc, char **argv) {
  yyin = fopen(argv[1], "r");
  if(yyin == NULL){
    fprintf(stderr, "impossible d'ouvrir le fichier %s\n", argv[1]);
    exit(1);
  }
  test_syntax_parser_internal(true);

  return 0;
}
