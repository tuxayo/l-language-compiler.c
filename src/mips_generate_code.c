/*
 * generate_mips_code.c
 *
 *  Created on: 23 mars 2015
 *      Author: Didier_TA
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "mips_generate_code.h"

#define INTEGER_SIZE (4)

int cpt = 0;

char label[100];

static bool should_display_mips = false;


void mips_init_display_mode(bool _should_display_mips) {
	should_display_mips = _should_display_mips;
}

void mips_begin_data_section() {
	if( !should_display_mips) return;

	puts("\t.data");
}

void mips_generate_word(int value) {
	if( ! should_display_mips) return;

	printf(".word %d\n", value);
}

void mips_generate_space(char *label, int nb_bytes) {
	if( ! should_display_mips) return;

	printf("%s:	.space %d\n", label, nb_bytes);
}

void mips_begin_text_section() {
	if( ! should_display_mips) return;

	printf("\n");
	puts("\t.text");
}

void mips_generate_start_function() {
	if( ! should_display_mips) return;

	puts("__start:");
	printf("\t");
	puts("jal \t main");
	printf("\t");
	puts("li \t $v0, 10");
	printf("\t");
	puts("syscall \t # stop process execution");
}

void mips_begin_function(char *function_name) {
	if( ! should_display_mips) return;

	printf("%s :\n", function_name); // tag
	mips_push_register("$fp");
	puts("\tmove \t $fp, $sp \t\t # nouvelle valeur de $fp");
	mips_push_register("$ra");
}

void mips_end_function(int nb_local_var) {
	if( ! should_display_mips) return;

	mips_comment_indented("end function, begin deallocating local variables");
	mips_deallocate_words(nb_local_var);
	mips_comment_indented("finished finished deallocating local variables");
	mips_pop_to_register("$ra");
	mips_pop_to_register("$fp");
	mips_jr("$ra");
}


void mips_ecrire_instruction() {
	if( ! should_display_mips) return;

	printf("\t");
	// TODO : what does it serve?
}

void mips_alloc_word_in_stack() {
	if( ! should_display_mips) return;

	mips_subi("$sp", "$sp", INTEGER_SIZE);
}

void mips_dealloc_word_in_stack() {
	if( ! should_display_mips) return;

	mips_addi("$sp", "$sp", INTEGER_SIZE);

}

void mips_deallocate_words(int nb_args) {
	if( ! should_display_mips) return;

	for(int i = 0 ; i < nb_args ; ++i) {
		mips_dealloc_word_in_stack();
	}
}

void mips_push_register(char *register_) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("# empile le registre %s\n", register_);

	mips_alloc_word_in_stack();

	mips_sw_with_offset(register_, "$sp", 0);
	printf("\n");
}

void mips_pop_operands_to_registers(char *left_operand, char *right_operand) {
	mips_pop_to_register(right_operand);
	mips_pop_to_register(left_operand);
}

void mips_pop_to_register(char *register_) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("# depile vers registre %s\n", register_);

	mips_lw(register_, "0($sp)");

	mips_addi("$sp", "$sp", INTEGER_SIZE);
	printf("\n");
}

void mips_sw_with_offset(char *register_, char *addr, int offset) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("sw \t %s, %d(%s) \n", register_, offset, addr);
}

void mips_sw(char *register_, char *var_label) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("sw \t %s, %s \n", register_, var_label);
}

void mips_lw_with_offset(char *register_, char *addr, int offset) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("lw \t %s, %d(%s) \n", register_, offset, addr);
}

void mips_lw(char *register_, char *addr) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("lw \t %s, %s \n", register_, addr);
}

void mips_addi(char *dest_register, char *arg1, int arg2) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("addi \t %s, %s, %d \n", dest_register, arg1, INTEGER_SIZE);
}

void mips_add(char *dest_register, char *register1, char *register2) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("add \t %s, %s, %s \n", dest_register, register1, register2);
}

void mips_subi(char *dest_register, char *arg1, int arg2) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("subi \t %s, %s, %d \n", dest_register, arg1, INTEGER_SIZE);
}

void mips_sub(char *dest_register, char *register1, char *register2) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("sub \t %s, %s, %s \n", dest_register, register1, register2);
}

void mips_move(char *dest_register, char *source_register) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("# nouvelle valeur de %s \n", dest_register);

	printf("\t");
	printf("move \t %s, %s", dest_register, source_register);
}

void mips_li(char *dest_register, int value) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("li \t %s, %d \n", dest_register, value);
}


void mips_syscall() {
	if( ! should_display_mips) return;

	printf("\t");
	printf("syscall\n");
	printf("\n");
}

void mips_jr(char *register_) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("jr  \t  %s \n", register_);
}

void mips_j(char *label) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("j \t  %s \n", label);
}

void mips_jal(char *label) {
	if( ! should_display_mips) return;


	printf("\t");
	printf("jal \t  %s # call function\n", label);
}


void mips_flush_write() {
	if( ! should_display_mips) return;

	printf("\t");
	printf("# End of line\n");

	printf("\t");
	printf("li  \t $a0, \'\\n\' \n");
	mips_li("$v0", 11);
	mips_syscall();
}

int get_local_var_offset_from_fp(int relative_address) {
	return -8 - relative_address;
}

int get_argument_offset_from_fp(int relative_address, int nb_args) {
	return 4*nb_args - relative_address;
}

int get_return_value_offset_from_fp(int nb_args) {
	return 4 * (nb_args + 1);
}


void mips_multiply(char *dest_register, char *register1, char *register2) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("mult \t %s, %s\n", register1, register2);
	mips_mflo(dest_register);
}

void mips_divide(char *dest_register, char *register1, char *register2) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("div \t %s, %s\n", register1, register2);
	mips_mflo(dest_register);

}

void mips_mfhi(char *dest_register) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("mfhi \t %s\n", dest_register);

}

void mips_mflo(char *dest_register) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("mflo \t %s\n", dest_register);

}

void mips_beq(char *register1, char *register2, char *addr) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("beq \t %s, %s, %s \n", register1, register2, addr);

}

void mips_bne(char *register1, char *register2, char *addr) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("bne \t %s, %s, %s \n", register1, register2, addr);

}

void mips_blt(char *register1, char *register2, char *addr) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("blt \t %s, %s, %s \n", register1, register2, addr);

}

void mips_ble(char *register1, char *register2, char *addr) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("ble \t %s, %s, %s \n", register1, register2, addr);

}

void create_label(char* label) {
	if( ! should_display_mips) return;

	sprintf(label, "e%d", ++cpt);
}

void mips_display_label(char *label) {
	if( ! should_display_mips) return;

	printf("%s: \n", label);
}

void mips_display_label_with_comment(char *label, char* comment) {
	if( ! should_display_mips) return;

	printf("%s: # %s\n", label, comment);
}

void mips_comment(char* comment) {
	if( ! should_display_mips) return;

	printf("# %s\n", comment);
}

void mips_comment_indented(char* comment) {
	if( ! should_display_mips) return;

	printf("\t# %s\n", comment);
}

void mips_or(char *dest_register, char *register1, char *register2) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("or \t %s, %s, %s\n", dest_register, register1, register2);
}


void mips_and(char *dest_register, char *register1, char *register2) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("and \t %s, %s, %s\n", dest_register, register1, register2);
}

void mips_not(char *dest_register, char *register1) {
	if( ! should_display_mips) return;

	printf("\t");
	printf("not \t %s, %s\n", dest_register, register1);
}

void mips_convert_ind_to_bytes(char *register_containing_ind) {
	if ( ! should_display_mips) return;
	printf("\t");
	mips_comment("conversion indice du tableau en octets");
	mips_add(register_containing_ind, register_containing_ind, register_containing_ind);
	mips_add(register_containing_ind, register_containing_ind, register_containing_ind);

}

