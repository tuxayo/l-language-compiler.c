/* symboles non terminaux */

#define EPSILON 0

#define NB_NON_TERMINAUX 41

#define _listeDecVariables_ 1
#define _listeDecFonctions_ 2
#define _declarationVariable_ 3
#define _declarationFonction_ 4
#define _listeParam_ 5
#define _listeInstructions_ 6
#define _instruction_ 8
#define _instructionAffect_ 9  
#define _instructionBloc_ 10
#define _instructionSi_ 11
#define _instructionTantque_ 12 
#define _instructionAppel_ 13
#define _instructionRetour_ 14
#define _instructionEcriture_ 15
#define _instructionVide_ 16
#define _var_ 17
#define _expression_ 18
#define _appelFct_ 19
#define _conjonction_ 20
#define _negation_ 21
#define _comparaison_ 22
#define _expArith_ 23
#define _terme_ 24
#define _facteur_ 25
#define _argumentsEffectifs_ 26 
#define _listeExpressions_ 27
#define _listeExpressionsBis_ 7
#define _programme_ 28
#define _conjonctionBis_ 29
#define _optTailleTableau_ 30
#define _expArithBis_ 31
#define _optSinon_ 32
#define _comparaisonBis_ 33
#define _optDecVariables_ 34
#define _optIndice_ 35
#define _listeDecVariablesBis_ 36
#define _instructionPour_ 37
#define _termeBis_ 38
#define _expressionBis_ 39
#define _instructionFaire_ 40
#define _optListeDecVariables_ 41



/* symboles terminaux */
#define NB_TERMINAUX 31

#define VIRGULE 1
#define POINT_VIRGULE 2
#define PLUS 3
#define MOINS 4
#define FOIS 5
#define DIVISE 6
#define PARENTHESE_OUVRANTE 7
#define PARENTHESE_FERMANTE 8
#define CROCHET_OUVRANT 9
#define CROCHET_FERMANT 10
#define ACCOLADE_OUVRANTE 11
#define ACCOLADE_FERMANTE 12
#define EGAL 13
#define INFERIEUR 14
#define ET 15
#define OU 16
#define NON 17

// keywords
#define SI 18
#define ALORS 19
#define SINON 20
#define TANTQUE 21
#define FAIRE 22
#define ENTIER 23
#define RETOUR 24
#define LIRE 25
#define ECRIRE 26
#define POUR 27
//end of keywords

#define ID_VAR 28
#define ID_FCT 29
#define NOMBRE 30


#define FIN 31


