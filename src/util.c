#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "util.h"
#include "analyseur_lexical.h"

/*-------------------------------------------------------------------------*/

extern int nb_ligne;
extern bool display_sytax_tree;
int indent_xml = 0;
int indent_step = 1; // set to 0 for no indentation

/*-------------------------------------------------------------------------*/

void erreur(char *message) {
  fflush(stdout); // to ensure error message is not above XML tree
  fprintf (stderr, "Ligne %d : ", nb_ligne);
  fprintf (stderr, "%s\n", message);
  exit(1);
}

// for semantic errors, when using abstract tree, we can't display
// the ligne for now, so we need this other function
void erreur_semantique(char *message) {
  fflush(stdout); // to ensure error message is not above XML tree
  fprintf (stderr, "Error %s\n", message);
  exit(1);
}

/*-------------------------------------------------------------------------*/

void debug_dump_token(int token) {
  char nom [100];
  char valeur[100];
  nom_token(token, nom, valeur);
  printf("token{nom: %s, valeur: %s}\n", nom, valeur);
}

/*-------------------------------------------------------------------------*/

char *duplique_chaine(char *src) {
  char *dest = malloc(sizeof(char) * strlen(src));
  strcpy(dest, src);
  return dest;
}

/*-------------------------------------------------------------------------*/

void indent() {
    int i;
    for( i = 0; i < indent_xml; i++ ) {
      printf( "  " );
    }    
}
/*-------------------------------------------------------------------------*/

void affiche_balise_ouvrante(const char *fct_, int trace_xml) {
  if( trace_xml ) {
    indent();
    indent_xml += indent_step ;
	  fprintf (stdout, "<%s>\n", fct_);
	}
}

/*-------------------------------------------------------------------------*/

void afficher_balise_ouvrante(const char *message) {
	affiche_balise_ouvrante(message, display_sytax_tree);
}

/*-------------------------------------------------------------------------*/

void affiche_balise_fermante(const char *fct_, int trace_xml) {
  if(trace_xml) {
    indent_xml -= indent_step ;
    indent();
    fprintf (stdout, "</%s>\n", fct_);
  }
}

/*-------------------------------------------------------------------------*/

void afficher_balise_fermante(const char *message) {
	affiche_balise_fermante(message, display_sytax_tree);
}

/*-------------------------------------------------------------------------*/

void affiche_texte(char *texte_, int trace_xml) {
  if(trace_xml) {
    indent();
    fprintf (stdout, "%s\n", texte_);
  }
}

/*-------------------------------------------------------------------------*/

void afficher_texte(char *texte_) {
  affiche_texte(texte_, display_sytax_tree);
}

/*-------------------------------------------------------------------------*/

// display with the format "<mot_clef>entier</mot_clef>"
void afficher_symbole_terminal(int token) {
  char nom [100];
  char valeur[100];
  char symbole_terminal_formatted[200];
  nom_token(token, nom, valeur);
  sprintf(symbole_terminal_formatted, "<%s>%s</%s>", nom, valeur, nom);
  afficher_texte(symbole_terminal_formatted);
}

/*-------------------------------------------------------------------------*/

void affiche_xml_texte( char *texte_ ) {
  int i = 0;
  while( texte_[ i ] != '\0' ) {
    if( texte_[ i ] == '<' ) {
      fprintf( stdout, "&lt;" );
    }
    else if( texte_[ i ] == '>' ) {
      fprintf( stdout, "&gt;" );
    }
    else if( texte_[ i ] == '&' ) {
      fprintf( stdout, "&amp;" );
    }
    else {
      putchar( texte_[i] );
    }
    i++;
  }
}

/*-------------------------------------------------------------------------*/

void affiche_element(char *fct_, char *texte_, int trace_xml) {
  if(trace_xml) {
    indent();
    fprintf (stdout, "<%s>", fct_ );
    affiche_xml_texte( texte_ );
    fprintf (stdout, "</%s>\n", fct_ );
  }
}
