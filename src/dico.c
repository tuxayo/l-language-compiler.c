#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "util.h"
#include "dico.h"

extern char *message_error;

/*-------------------------------------------------------------------------*/

void entreeFonction(void){
	dico.base = dico.sommet;
	contexte = C_VARIABLE_LOCALE;
	adresseLocaleCourante = 0;
	adresseArgumentCourant = 0;
}

/*-------------------------------------------------------------------------*/

void sortieFonction(void){
	dico.sommet = dico.base;
	dico.base = 0;
	contexte = C_VARIABLE_GLOBALE;
}

/*-------------------------------------------------------------------------*/

int ajouteIdentificateur(char *identif, int classe, int type, int adresse, int complement)
{
	dico.tab[dico.sommet].identif = strdup(identif);
	dico.tab[dico.sommet].classe = classe;
	dico.tab[dico.sommet].type = type;
	dico.tab[dico.sommet].adresse = adresse;
	dico.tab[dico.sommet].complement = complement;
	dico.sommet++;

	if(dico.sommet == maxDico) {
		fprintf(stderr, "attention, plus de place dans le dictionnaire des \
						 identificateurs, augmenter maxDico\n");
		exit(1);
	}
	/* affiche_dico(); */
	return dico.sommet - 1;
}

/*-------------------------------------------------------------------------*/
// for identifiers found in executable part (inside function)
int rechercheExecutable(char *identif)
{
	int i;
	for(i = dico.sommet - 1; i >= 0; i--){
		if(!strcmp(identif, dico.tab[i].identif))
			return i;
	}
	return -1;
}

/*-------------------------------------------------------------------------*/
// for identifiers found in declarative parts (declarations)
int rechercheDeclarative(char *identif)
{
	int i;
	for(i = dico.base; i < dico.sommet; i++){
		if(!strcmp(identif, dico.tab[i].identif))
			return i;
	}
	return -1;
}

/*-------------------------------------------------------------------------*/

bool function_in_symbol_table(char *function_name) {
	return rechercheExecutable(function_name) != -1;
}

/*-------------------------------------------------------------------------*/


bool var_is_local_or_arg(char *var_name) {
	int var_index = rechercheExecutable(var_name);
	int var_scope = dico.tab[var_index].classe;
	return var_scope == C_VARIABLE_LOCALE ||
		   var_scope == C_ARGUMENT;
}

bool var_is_local(char *var_name) {
	int var_index = rechercheExecutable(var_name);
	if(var_index == -1) return false;
	int var_scope = dico.tab[var_index].classe;
	return var_scope == C_VARIABLE_LOCALE;
}


bool var_is_declared(char *var_name) {
	return rechercheExecutable(var_name) != -1;
}

bool var_not_global(char *var_name) {
	return !var_is_global(var_name);
}

bool var_is_global(char *var_name) {
	int var_index = rechercheExecutable(var_name);
	if(var_index == -1) return false;
	int var_scope = dico.tab[var_index].classe;
	return var_scope == C_VARIABLE_GLOBALE;
}

bool var_is_tableau(char *var_name) {
	int var_index = rechercheExecutable(var_name);
	if((var_index) < 0) return false;

	int var_type = dico.tab[var_index].type;
	return var_type == T_TABLEAU_ENTIER;
}

int get_var_address(char *var_name) {
	int var_ind_in_symbol_table = rechercheExecutable(var_name);
	// var already declared so never == -1 => no need to check
	return dico.tab[var_ind_in_symbol_table].adresse;
}

void affiche_dico(void)
{
	int i;
	printf("------------------------------------------\n");
	printf("base = %d\n", dico.base);
	printf("sommet = %d\n", dico.sommet);

	for(i=0; i < dico.sommet; i++){
		printf("%d ", i);
		printf("%s ", dico.tab[i].identif);
		if(dico.tab[i].classe == C_VARIABLE_GLOBALE)
			printf("GLOBALE ");
		else
		if(dico.tab[i].classe == C_VARIABLE_LOCALE)
			printf("LOCALE ");
		else
		if(dico.tab[i].classe == C_ARGUMENT)
			printf("ARGUMENT ");

		if(dico.tab[i].type == T_ENTIER)
			printf("ENTIER ");
		else if(dico.tab[i].type == T_TABLEAU_ENTIER)
			printf("TABLEAU ");
		/*     else if(dico.tab[i].type == _ARGUMENT) */
		/*       printf("ARGUMENT "); */
		else if(dico.tab[i].type == T_FONCTION)
			printf("FONCTION ");

		printf("%d ", dico.tab[i].adresse);
		printf("%d\n", dico.tab[i].complement);
	}

	printf("------------------------------------------\n");
}

int calculate_address(int contexte, int size_new_var) {
	if(contexte == C_VARIABLE_GLOBALE) {
		int current_address = adresseGlobaleCourante;
		adresseGlobaleCourante += size_new_var;
		return current_address;

	} else if(contexte == C_ARGUMENT) {
		int current_address = adresseArgumentCourant;
		adresseArgumentCourant += size_new_var;
		return current_address;

	} else if(contexte == C_VARIABLE_LOCALE) {
		int current_address = adresseLocaleCourante;
		adresseLocaleCourante += size_new_var;
		return current_address;

	} else {
		puts("Context error");
		exit(EXIT_FAILURE);
	}
}
