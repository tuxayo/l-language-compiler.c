/*
 * util_parcours_arbre_abstrait.h
 *
 *  Created on: 12 avr. 2015
 *      Author: Didier_TA
 */

#ifndef SRC_UTIL_PARCOURS_ARBRE_ABSTRAIT_H_
#define SRC_UTIL_PARCOURS_ARBRE_ABSTRAIT_H_

#include <stdio.h>
#include <stdbool.h>

#include "util.h"
#include "syntabs.h"
#include "dico.h"
#include "mips_generate_code.h"
#include "parcours_arbre_abstrait.h"

void handle_tree_display(enum output_mode _output_mode);
void check_matching_declared_type(n_var* n, int used_type);
void check_matching_call_arguments(int index_in_symbol_table, n_l_exp* call_arg_list);
void check_presence_of_main_function();
int count_nb_args_in_function(n_dec *n);
int count_nb_local_vars_in_function(n_dec *n);
int count_nb_exp_in_list(n_l_exp *n);
void opExp_process_operands(n_exp* n);
void opExp_process_operands_with_label(n_exp* n, char *label);
void generate_mips_for_simple_global_var(char *var_name, enum var_context context);
void generate_mips_simple_local_var(char *var_name, enum var_context context);
void generate_mips_for_simple_var(char *var_name, enum var_context context);
void generate_mips_arith_expression(n_exp* n, operation op);
void generate_mips_comparison(n_exp* n, operation op);
void generate_mips_proposition(n_exp *n, operation op);
void add_function_id_in_symbol_table(n_dec *n);
void generate_mips_instruction_retour();
void generate_mips_instruction_ecrire();
void generate_mips_non_opExp(n_exp *n);
void generate_mips_for_arguments(char *var_name, enum var_context context);
void generate_mips_for_table(char *var_name, enum var_context context);

extern char message_error[100];

#endif /* SRC_UTIL_PARCOURS_ARBRE_ABSTRAIT_H_ */
