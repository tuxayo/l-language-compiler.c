#ifndef __PARCOURS_ARBRE_ABSTRAIT__
#define __PARCOURS_ARBRE_ABSTRAIT__

#include <stdbool.h>

#include "syntabs.h"



#define LABEL_SIZE (10)

enum output_mode{abstract_tree, symbol_table, mips};
enum var_context{l_value, r_value};

void parcours_n_prog(n_prog *n, enum output_mode _output_mode);

void parcours_l_instr(n_l_instr *n);
void parcours_instr(n_instr *n);
void parcours_instr_si(n_instr *n);
void parcours_instr_tantque(n_instr *n);
void parcours_instr_faire(n_instr *n);      /* MODIFIE POUR EVAL */
void parcours_instr_pour(n_instr *n);       /* MODIFIE POUR EVAL */
void parcours_instr_affect(n_instr *n);
void parcours_instr_appel(n_instr *n);
void parcours_instr_retour(n_instr *n);
void parcours_instr_ecrire(n_instr *n);
void parcours_l_exp(n_l_exp *n);
void parcours_exp(n_exp *n);
void parcours_varExp(n_exp *n);
void parcours_opExp(n_exp *n);
void parcours_intExp(n_exp *n);
void parcours_lireExp(n_exp *n);
void parcours_appelExp(n_exp *n);
void parcours_l_dec(n_l_dec *n);
void parcours_dec(n_dec *n);
void parcours_foncDec(n_dec *n);
void parcours_varDec(n_dec *n);
void parcours_tabDec(n_dec *n);
void parcours_var(n_var *n, enum var_context _var_context);
void parcours_var_simple(n_var *n, enum var_context _var_context);
void parcours_var_indicee(n_var *n, enum var_context _var_context);
void parcours_appel(n_appel *n);

bool display_tree;
bool display_symbol_table;
bool display_mips;

int contexte;

int nb_args_current_function;
int nb_local_var_current_function;

int adresseLocaleCourante;
int adresseArgumentCourant;
int adresseGlobaleCourante;



#endif
