#ifndef __UTIL__
#define __UTIL__

#define YYTEXT_MAX 100

char *duplique_chaine(char *s);
void erreur(char *message);
void erreur_semantique(char *message);
void debug_dump_token(int token);
void affiche_balise_ouvrante(const char *fct_, int trace_xml);
void affiche_balise_fermante(const char *fct_, int trace_xml);
void afficher_balise_ouvrante(const char *message);
void afficher_balise_fermante(const char *message);
void affiche_element(char *fct_, char *texte_, int trace_xml);
void affiche_texte(char *texte_, int trace_xml);
void afficher_symbole_terminal(int token);


#endif
