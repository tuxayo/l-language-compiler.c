/*
 * util_parcours_arbre_abstrait.c
 *
 *  Created on: 12 avr. 2015
 *      Author: Didier_TA
 */
#include <string.h>
#include <stdlib.h>
#include "util_parcours_arbre_abstrait.h"
#include "syntabs.h"
#include "dico.h"


extern int nb_args_current_function;

/***************************************************************************/

int count_nb_args_in_function(n_dec *n) {
	int nb_args = 0;

	if(n->u.foncDec_.param == NULL) return 0;

	n_l_dec *next_element = n->u.foncDec_.param->queue;

	while(1) {
		++nb_args;
		if(next_element == NULL) break;
		next_element = next_element->queue;
	}

	return nb_args;
}

/***************************************************************************/

int count_nb_exp_in_list(n_l_exp *n) {
	int nb_exp = 0;

	if(n == NULL) return 0;

	n_l_exp *next_element = n->queue;

	while(1) {
		++nb_exp;
		if(next_element == NULL) break;
		next_element = next_element->queue;
	}

	return nb_exp;
}

/***************************************************************************/
// TODO remove duplication between count_nb_args_in_function and count_nb_local_vars_in_function
int count_nb_local_vars_in_function(n_dec *n) {
	int nb_vars = 0;

	if(n->u.foncDec_.variables == NULL) return 0;

	n_l_dec *next_element = n->u.foncDec_.variables->queue;

	while(1) {
		++nb_vars;
		if(next_element == NULL) break;
		next_element = next_element->queue;
	}

	return nb_vars;
}

/****************************************************************************
 * 					TABLE MIPS CODE GENERATION
 ****************************************************************************/
void generate_mips_for_table(char *var_name, enum var_context context) {
	char comment[200];
	char addr[200];

	if(context == l_value) {
		sprintf(comment, "affect l-value %s", var_name);

	} else { // context == r_value
		sprintf(comment, "tab_var_r_value %s", var_name);

	}

	mips_comment_indented(comment);
	mips_pop_to_register("$t1"); // récupérer l'indice stockée dans la pile
	mips_convert_ind_to_bytes("$t1");
	sprintf(addr, "%s($t1)", var_name);

	if(context == l_value) {
		mips_sw("$t0", addr);
	} else { // context == r_value
		mips_lw("$t0", addr); // récupérer la valeur dans le tableau
		mips_push_register("$t0");
	}

}

void check_matching_declared_type(n_var* n, int used_type) {
	int ind_in_symbol_table = rechercheExecutable(n->nom);
	int type_of_var_declared = dico.tab[ind_in_symbol_table].type;
	if(type_of_var_declared != used_type) {
		sprintf(message_error,
				"mauvaise utilisation de la variable %s (vérifier son type)",
				n->nom);
		erreur_semantique(message_error);
	}
}

/****************************************************************************
 * 					CHECKING TREE'S STRUCTURE
 ****************************************************************************/

void check_matching_call_arguments(int index_in_symbol_table, n_l_exp* call_arg_list) {
	int nb_call_args = count_nb_exp_in_list(call_arg_list);
	int nb_args_declared = dico.tab[index_in_symbol_table].complement;
	if(nb_call_args != nb_args_declared) {
		sprintf(message_error,
				"mauvaise utilisation de la variable %s (nombre d'arguments different avec la declaration)",
				dico.tab[index_in_symbol_table].identif);
		erreur_semantique(message_error);
	}
}

/***************************************************************************/

void check_presence_of_main_function() {
	if(rechercheExecutable("main") == -1) {
		erreur_semantique("Missing main function");
	}
}


/****************************************************************************
 * 					SIMPLE VAR MIPS CODE GENERATION
 ****************************************************************************/

void generate_mips_for_simple_var(char *var_name, enum var_context context) {
//	char comment[200];
	if(var_is_global(var_name)) {
		generate_mips_for_simple_global_var(var_name, context);

	} else if(var_is_local(var_name)) {
		generate_mips_simple_local_var(var_name, context);

	} else {
		generate_mips_for_arguments(var_name, context);


	}
}

/***************************************************************************/

void generate_mips_simple_local_var(char *var_name, enum var_context context) {
	char comment[200];
	if(context == l_value) {
		sprintf(comment, "affect to local var %s", var_name);

	} else { // context == r_value
		sprintf(comment, "load local var %s", var_name);

	}

	mips_comment(comment);
	int relative_address = get_var_address(var_name);
	int var_offset = get_local_var_offset_from_fp(relative_address);

	if(context == l_value) {
		mips_sw_with_offset("$t0", "$fp", var_offset);

	} else { // context == r_value
		mips_lw_with_offset("$t0", "$fp", var_offset);
		mips_push_register("$t0");
	}
}

/***************************************************************************/

void generate_mips_for_simple_global_var(char *var_name, enum var_context context) {
	char comment[200];
	if(context == l_value) {
		sprintf(comment, "affect to global var %s", var_name);
	} else { // context == r_value
		sprintf(comment, "load global var %s", var_name);
	}

	mips_comment(comment);

	if(context == l_value) {
		mips_sw("$t0", var_name);	   // $a = $t0

	} else { // context == r_value
		mips_lw("$t0", var_name);
		mips_push_register("$t0");
	}
}

/***************************************************************************/

void generate_mips_for_arguments(char *var_name, enum var_context context) {
	char comment[200];
	if(context == l_value) {
		sprintf(message_error, "l'argument %s ne peut pas être à gauche d'une affectation\n", var_name);
		erreur_semantique(message_error);
	} else { // context == r_value
		sprintf(comment, "load argument %s", var_name);
		mips_comment(comment);
		int relative_address = get_var_address(var_name);
		int var_offset = get_argument_offset_from_fp(relative_address, nb_args_current_function);

		mips_lw_with_offset("$t0", "$fp", var_offset);
		mips_push_register("$t0");
	}
}

/****************************************************************************
 * 					HANDLE OPEXP FUNCTION OPERANDS
 ****************************************************************************/

void opExp_process_operands_with_label(n_exp* n, char *label)
{
	if (n->u.opExp_.op1 != NULL) {
		parcours_exp (n->u.opExp_.op1);

		/* handle 'court-circuit' */
		if(n->u.opExp_.op == ou) {
			mips_pop_to_register("$t0"); // get result of left exp
			mips_push_register("$t0"); // the pile has to be left in the same state as it was
			mips_bne("$t0", "$zero", label); // label == label at the end of 'ou' of opExp

		} else if(n->u.opExp_.op == et) {
			mips_pop_to_register("$t0"); // get result of left exp
			mips_push_register("$t0");// the pile has to be left in the same state as it was
			mips_beq("$t0", "$zero", label);// label == label at the end of 'et' of opExp

		}
	}

	if (n->u.opExp_.op2 != NULL) {
		parcours_exp (n->u.opExp_.op2);
	}

}

/***************************************************************************/

void opExp_process_operands(n_exp* n) {
	if (n->u.opExp_.op1 != NULL) {
		parcours_exp (n->u.opExp_.op1);
	}
	if (n->u.opExp_.op2 != NULL) {
		parcours_exp (n->u.opExp_.op2);
	}
}

/***************************************************************************/

void generate_mips_arith_expression(n_exp* n, operation op) {
	opExp_process_operands(n);
	mips_pop_operands_to_registers("$t0", "$t1");

	switch(op) {
		case plus :
			mips_add("$t0", "$t0", "$t1");
			break;
		case moins :
			mips_sub("$t0", "$t0", "$t1");
			break;
		case fois :
			mips_multiply("$t0", "$t0", "$t1");
			break;
		case divise :
			mips_divide("$t0", "$t0", "$t1");
			break;
		default :
			puts("Error when calling : generate_mips_arith_expression()");
			exit(1);

	}

	mips_push_register("$t0");

}

/***************************************************************************/

void generate_mips_comparison(n_exp* n, operation op) {
	opExp_process_operands(n);
	mips_pop_operands_to_registers("$t0", "$t1");

	if(op == diff) {
		affiche_texte("non", display_tree);
	}

	mips_li("$t2", -1);
	char label[LABEL_SIZE];create_label(label);

	switch(op) {
		case egal :
			mips_beq("$t0", "$t1", label);
			break;
		case diff :
			mips_bne("$t0", "$t1", label);
			break;
		case inf :
			mips_blt("$t0", "$t1", label);
			break;
		case infeg :
			mips_ble("$t0", "$t1", label);
			break;
		default :
			puts("Error when calling : generate_mips_comparison()");
			exit(1);
	}


	mips_li("$t2", 0);

	mips_display_label(label);
	mips_push_register("$t2");
}

/***************************************************************************/

void generate_mips_proposition(n_exp *n, operation op) {
	char label[LABEL_SIZE];create_label(label);

	opExp_process_operands_with_label(n, label);
	mips_pop_operands_to_registers("$t0", "$t1");

	if(op == ou) {
		mips_or("$t0", "$t0", "$t1");

	} else if(op == et) {
		mips_and("$t0", "$t0", "$t1");
	}
	mips_push_register("$t0");

	// generate label for 'court-circuit'
	mips_display_label(label);
}

/***************************************************************************/
void generate_mips_non_opExp(n_exp *n) {
	opExp_process_operands(n);
	mips_pop_to_register("$t0");
	mips_not("$t1", "$t0");
	mips_push_register("$t1");

}



/***************************************************************************/
void add_function_id_in_symbol_table(n_dec *n) {
	nb_args_current_function = count_nb_args_in_function(n);
	ajouteIdentificateur(n->nom, C_FONCTION, T_FONCTION, 0, nb_args_current_function);

}

/***************************************************************************/

void generate_mips_instruction_retour() {
	mips_comment_indented("save return value");
	mips_pop_to_register("$t0");
	mips_sw_with_offset("$t0", "$fp", get_return_value_offset_from_fp(nb_args_current_function));
	mips_end_function(nb_local_var_current_function);
}

/***************************************************************************/

void generate_mips_instruction_ecrire() {
	mips_pop_to_register("$a0");
	mips_li("$v0", 1);
	mips_syscall();
}

/***************************************************************************/
void handle_tree_display(enum output_mode _output_mode) {
	display_tree = (_output_mode == abstract_tree);
	display_symbol_table = (_output_mode == symbol_table);
	if(_output_mode == mips) mips_init_display_mode(true);
}
