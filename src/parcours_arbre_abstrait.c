#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "util.h"
#include "syntabs.h"
#include "dico.h"
#include "mips_generate_code.h"
#include "parcours_arbre_abstrait.h"
#include "util_parcours_arbre_abstrait.h"

int adresseLocaleCourante = 0;
int adresseArgumentCourant = 0;
int adresseGlobaleCourante = 0;

char message_error[100];


/*-------------------------------------------------------------------------*/

void parcours_n_prog(n_prog *n, enum output_mode _output_mode)
{
	contexte = C_VARIABLE_GLOBALE;

	handle_tree_display(_output_mode);

	char *fct = "prog";
	affiche_balise_ouvrante(fct, display_tree);

	mips_begin_data_section();

	parcours_l_dec(n->variables);

	mips_begin_text_section();
	mips_generate_start_function();

	parcours_l_dec(n->fonctions);

	if(_output_mode == mips) {
		check_presence_of_main_function(_output_mode);
	}

	affiche_balise_fermante(fct, display_tree);

}

/*-------------------------------------------------------------------------*/

void parcours_l_instr(n_l_instr *n)
{
	char *fct = "l_instr";
	if(n){
		affiche_balise_ouvrante(fct, display_tree);
		parcours_instr(n->tete);
		parcours_l_instr(n->queue);
		affiche_balise_fermante(fct, display_tree);
	}
}

/*-------------------------------------------------------------------------*/

void parcours_instr(n_instr *n)
{
	if(n){
		if(n->type == blocInst) parcours_l_instr(n->u.liste);
		else if(n->type == affecteInst) parcours_instr_affect(n);
		else if(n->type == siInst) parcours_instr_si(n);
		else if(n->type == tantqueInst) parcours_instr_tantque(n);
		else if(n->type == faireInst) parcours_instr_faire(n);
		else if(n->type == pourInst) parcours_instr_pour(n);
		else if(n->type == appelInst) parcours_instr_appel(n);
		else if(n->type == retourInst) parcours_instr_retour(n);
		else if(n->type == ecrireInst) parcours_instr_ecrire(n);
	}
}

/*-------------------------------------------------------------------------*/

void parcours_instr_si(n_instr *n)
{  
	char *fct = "instr_si";
	affiche_balise_ouvrante(fct, display_tree);

	parcours_exp(n->u.si_.test);

	mips_pop_to_register("$t0");
	mips_comment("if test result == 0 then goto else:");
	char label_else[LABEL_SIZE];create_label(label_else);
	mips_beq("$t0", "$zero", label_else);

	parcours_instr(n->u.si_.alors);

	char label_end_if[LABEL_SIZE];create_label(label_end_if);
	mips_j(label_end_if);
	mips_display_label_with_comment(label_else, "else");

	if(n->u.si_.sinon) {
		parcours_instr(n->u.si_.sinon);
	}

	mips_display_label_with_comment(label_end_if, "end if");
	affiche_balise_fermante(fct, display_tree);
}

/*-------------------------------------------------------------------------*/

void parcours_instr_tantque(n_instr *n)
{
	char *fct = "instr_tantque";
	affiche_balise_ouvrante(fct, display_tree);

	char label_begin_loop[LABEL_SIZE];create_label(label_begin_loop);
	mips_display_label_with_comment(label_begin_loop, "begin loop");

	parcours_exp(n->u.tantque_.test);

	mips_pop_to_register("$t0");
	mips_comment("if test result == 0 then goto end loop:");
	char label_end_loop[LABEL_SIZE];create_label(label_end_loop);
	mips_beq("$t0", "$zero", label_end_loop);

	parcours_instr(n->u.tantque_.faire);

	mips_comment("goto begin loop");
	mips_j(label_begin_loop);
	mips_display_label_with_comment(label_end_loop, "end loop");

	affiche_balise_fermante(fct, display_tree);
}

/*-------------------------------------------------------------------------*/
// TODO: DEAD CODE, the "faire" part is processed as an instruction block
// which seems correct so instruction faire might be an old version relic
void parcours_instr_faire(n_instr *n)          	/* MODIFIE POUR EVAL */
{                                             	/* MODIFIE POUR EVAL */
	char *fct = "instr_faire";                  /* MODIFIE POUR EVAL */
	affiche_balise_ouvrante(fct, display_tree); /* MODIFIE POUR EVAL */
//	char label_begin_loop[LABEL_SIZE];generate_label(label_begin_loop);
//	mips_display_label_with_comment(label_begin_loop, "begin loop");
	parcours_instr(n->u.faire_.faire);          /* MODIFIE POUR EVAL */

	parcours_exp(n->u.faire_.test);             /* MODIFIE POUR EVAL */
	printf("############# INSTR_FAIRE : I AM NOT USELESS ################\n");
//	mips_depile_register("$t0");
//	mips_comment("if test result != 0 then goto begin loop:");
//	mips_bne("$t0", "$zero", label_begin_loop);
	affiche_balise_fermante(fct, display_tree); /* MODIFIE POUR EVAL */
}                                             	/* MODIFIE POUR EVAL */

/*-------------------------------------------------------------------------*/

void parcours_instr_pour(n_instr *n)                	/* MODIFIE POUR EVAL */
{                                                  		/* MODIFIE POUR EVAL */
	char *fct = "instr_pour";                        	/* MODIFIE POUR EVAL */
	affiche_balise_ouvrante(fct, display_tree);         /* MODIFIE POUR EVAL */

	mips_comment("init pour loop");

	parcours_instr(n->u.pour_.init);                  	/* MODIFIE POUR EVAL */

	char label_begin_iteration[LABEL_SIZE];create_label(label_begin_iteration);
	mips_display_label_with_comment(label_begin_iteration, "begin iteration");

	parcours_exp(n->u.pour_.test);                    	/* MODIFIE POUR EVAL */

	mips_pop_to_register("$t0");
	mips_comment("if test result == 0 then goto end loop:");
	char label_end_loop[LABEL_SIZE];create_label(label_end_loop);
	mips_beq("$t0", "$zero", label_end_loop);

	parcours_instr(n->u.pour_.faire);                 	/* MODIFIE POUR EVAL */
	parcours_instr(n->u.pour_.incr);                  	/* MODIFIE POUR EVAL */

	mips_j(label_begin_iteration);
	mips_display_label_with_comment(label_end_loop, "end loop");

	affiche_balise_fermante(fct, display_tree);         /* MODIFIE POUR EVAL */
}                                                  		/* MODIFIE POUR EVAL */

/*-------------------------------------------------------------------------*/

void parcours_instr_affect(n_instr *n)
{
	char *fct = "instr_affect";
	affiche_balise_ouvrante(fct, display_tree);

	char var_name[100];
	strcpy(var_name, n->u.affecte_.var->nom);

	int ind = rechercheExecutable(var_name);
	if(ind == -1) {
		sprintf(message_error, "la variable %s n'a pas été déclarée...\n", var_name);
		erreur_semantique(message_error);
	}

	parcours_var(n->u.affecte_.var, l_value);
	parcours_exp(n->u.affecte_.exp);

	mips_pop_to_register("$t0"); // valeur de l'operande droite du =

	if(var_is_tableau(var_name)) {
		generate_mips_for_table(var_name, l_value);

	} else { // var is simple
		generate_mips_for_simple_var(var_name, l_value);
	}

	affiche_balise_fermante(fct, display_tree);
}

/*-------------------------------------------------------------------------*/

void parcours_instr_appel(n_instr *n)
{
	char *fct = "instr_appel";
	affiche_balise_ouvrante(fct, display_tree);

	parcours_appel(n->u.appel);
	mips_dealloc_word_in_stack(); // dealloc the unused return value
	affiche_balise_fermante(fct, display_tree);
}
/*-------------------------------------------------------------------------*/
void parcours_appel(n_appel *n)
{
	int ind = rechercheExecutable(n->fonction);
	if(ind == -1) {
		sprintf(message_error, "function %s is not declared before use\n", n->fonction);
		erreur_semantique(message_error);
	}

	char *xml_tag_name = "appel";
	affiche_balise_ouvrante(xml_tag_name, display_tree);
	affiche_texte(n->fonction, display_tree);
	check_matching_call_arguments(ind, n->args);

	mips_comment_indented("alloc function return value before call");
	mips_alloc_word_in_stack();
	parcours_l_exp(n->args); // push args on stack
	mips_jal(n->fonction);

	int nb_args_in_function = dico.tab[ind].complement;
	mips_deallocate_words(nb_args_in_function);

	affiche_balise_fermante(xml_tag_name, display_tree);
}

/*-------------------------------------------------------------------------*/

void parcours_instr_retour(n_instr *n)
{
	char *fct = "instr_retour";
	affiche_balise_ouvrante(fct, display_tree);
	parcours_exp(n->u.retour_.expression);
	generate_mips_instruction_retour();
	affiche_balise_fermante(fct, display_tree);
}

/*-------------------------------------------------------------------------*/

void parcours_instr_ecrire(n_instr *n)
{
	char *fct = "instr_ecrire";
	affiche_balise_ouvrante(fct, display_tree);
	parcours_exp(n->u.ecrire_.expression);

	generate_mips_instruction_ecrire();

	mips_flush_write();

	affiche_balise_fermante(fct, display_tree);
}

/*-------------------------------------------------------------------------*/

void parcours_l_exp(n_l_exp *n)
{
	char *fct = "l_exp";
	affiche_balise_ouvrante(fct, display_tree);

	if(n){
		parcours_exp(n->tete);
		parcours_l_exp(n->queue);
	}
	affiche_balise_fermante(fct, display_tree);
}

/*-------------------------------------------------------------------------*/

void parcours_exp(n_exp *n)
{
	if(n->type == varExp) parcours_varExp(n);
	else if(n->type == opExp) parcours_opExp(n);
	else if(n->type == intExp) parcours_intExp(n);
	else if(n->type == appelExp) parcours_appelExp(n);
	else if(n->type == lireExp) parcours_lireExp(n);

}


/*-------------------------------------------------------------------------*/
void parcours_opExp(n_exp *n)
{
	char *fct = "opExp";
	affiche_balise_ouvrante(fct, display_tree);

	if(n->u.opExp_.op == plus) {
		affiche_texte("plus", display_tree);
		generate_mips_arith_expression(n, plus);

	} else if(n->u.opExp_.op == moins) {
		affiche_texte("moins", display_tree);
		generate_mips_arith_expression(n, moins);

	} else if(n->u.opExp_.op == fois) {
		affiche_texte("fois", display_tree);
		generate_mips_arith_expression(n, fois);

	} else if(n->u.opExp_.op == divise) {
		affiche_texte("divise", display_tree);
		generate_mips_arith_expression(n, divise);

	} else if(n->u.opExp_.op == egal) {
		affiche_texte("egal", display_tree);
		generate_mips_comparison(n, egal);

	} else if(n->u.opExp_.op == diff) {
		affiche_texte("diff", display_tree);
		generate_mips_comparison(n, diff);

	} else if(n->u.opExp_.op == inf) {
		affiche_texte("inf", display_tree);
		generate_mips_comparison(n, inf);

	} else if(n->u.opExp_.op == infeg) {
		affiche_texte("infeg", display_tree);
		generate_mips_comparison(n, infeg);

	} else if(n->u.opExp_.op == ou) {
		affiche_texte("ou", display_tree);
		generate_mips_proposition(n, ou);

	} else if(n->u.opExp_.op == et) {
		affiche_texte("et", display_tree);
		generate_mips_proposition(n, et);

	} else if(n->u.opExp_.op == non) {
		affiche_texte("non", display_tree);
		generate_mips_non_opExp(n);

	}

	affiche_balise_fermante(fct, display_tree);
}

/*-------------------------------------------------------------------------*/

void parcours_intExp(n_exp *n)
{
	char texte[ 50 ]; // Max. 50 chiffres
	sprintf(texte, "%d", n->u.entier);
	affiche_element( "intExp", texte, display_tree );

	mips_li("$t0", n->u.entier);
	mips_push_register("$t0");

}

/*-------------------------------------------------------------------------*/
void parcours_lireExp(n_exp *n)
{
	char *fct = "lireExp";
	affiche_balise_ouvrante(fct, display_tree);

	mips_li("$v0", 5);
	mips_syscall();
	mips_push_register("$v0");

	affiche_balise_fermante(fct, display_tree);
}

/*-------------------------------------------------------------------------*/

void parcours_appelExp(n_exp *n)
{
	char *fct = "appelExp";
	affiche_balise_ouvrante(fct, display_tree);
	parcours_appel(n->u.appel);

	affiche_balise_fermante(fct, display_tree);
}

/*-------------------------------------------------------------------------*/
void parcours_varExp(n_exp *n)
{
	char *fct = "varExp";
	affiche_balise_ouvrante(fct, display_tree);
	parcours_var(n->u.var, r_value);
	affiche_balise_fermante(fct, display_tree);
}
/*-------------------------------------------------------------------------*/

void parcours_var(n_var *n, enum var_context _var_context)
{
	if(n->type == simple) {
		parcours_var_simple(n, _var_context);
	}
	else if(n->type == indicee) {
		parcours_var_indicee(n, _var_context);
	}
}



/*-------------------------------------------------------------------------*/
void parcours_var_simple(n_var *n, enum var_context _var_context)
{
	affiche_element("var_simple", n->nom, display_tree);

	if(! var_is_declared(n->nom)) {
		sprintf(message_error, "la variable simple %s n'a pas été déclarée!", n->nom);
		erreur_semantique(message_error);
	}

	check_matching_declared_type(n, T_ENTIER);
	if(_var_context == r_value) { // there is nothing to do for l-value here
		generate_mips_for_simple_var(n->nom, r_value);// the store takes place in "instruction affection"

	}
}

/*-------------------------------------------------------------------------*/
void parcours_var_indicee(n_var *n, enum var_context _var_context)
{

	char *fct = "var_indicee";
	affiche_balise_ouvrante(fct, display_tree);

	// check declared
	if(! var_is_declared(n->nom)) {
		sprintf(message_error, "la variable indicée %s n'a pas été déclarée!", n->nom);
		erreur_semantique(message_error);
	}

	check_matching_declared_type(n, T_TABLEAU_ENTIER);
	affiche_element("var_base_tableau", n->nom, display_tree);

	parcours_exp( n->u.indicee_.indice );

	affiche_balise_fermante(fct, display_tree);

	if(_var_context == r_value) {
//		tab_var_r_value(n);
		generate_mips_for_table(n->nom, r_value);

	}
}

/*-------------------------------------------------------------------------*/

void parcours_l_dec(n_l_dec *n)
{
	char *fct = "l_dec";

	if( n ) {
		affiche_balise_ouvrante(fct, display_tree);
		parcours_dec(n->tete);
		parcours_l_dec(n->queue);
		affiche_balise_fermante(fct, display_tree);
	}
}

/*-------------------------------------------------------------------------*/

void parcours_dec(n_dec *n)
{
	if(n){
		if(n->type == foncDec) {
			parcours_foncDec(n);
		}
		else if(n->type == varDec) {
			parcours_varDec(n);
		}
		else if(n->type == tabDec) {
			parcours_tabDec(n);
		}
	}
}
/*-------------------------------------------------------------------------*/
void parcours_foncDec(n_dec *n) {
	char *fct = "foncDec";
	affiche_balise_ouvrante(fct, display_tree);

	if(function_in_symbol_table(n->nom)) {
		sprintf(message_error, "%s already declared", n->nom);
		erreur_semantique(message_error);
	}

	add_function_id_in_symbol_table(n);
	mips_begin_function(n->nom);
	entreeFonction();

	affiche_texte(n->nom, display_tree);

	contexte = C_ARGUMENT;
	parcours_l_dec(n->u.foncDec_.param);

	contexte = C_VARIABLE_LOCALE;
	nb_local_var_current_function = count_nb_local_vars_in_function(n);
	parcours_l_dec(n->u.foncDec_.variables);
	parcours_instr(n->u.foncDec_.corps);

	// return value: now it does nothing if return is forgotten in l code,
	// so there will be random data for the caller
	mips_end_function(nb_local_var_current_function);
	if (display_symbol_table) {
		affiche_dico();

	}

	sortieFonction();
	affiche_balise_fermante(fct, display_tree);

}

/*-------------------------------------------------------------------------*/
void parcours_varDec(n_dec *n) {
	if(var_is_local_or_arg(n->nom)) {
		sprintf(message_error, "%s already declared", n->nom);
		erreur_semantique(message_error);
	}

	if(contexte == C_VARIABLE_GLOBALE && var_is_global(n->nom)) {
		sprintf(message_error, "global var %s already declared", n->nom);
		erreur_semantique(message_error);
	}


	int address = calculate_address(contexte, INTEGER_SIZE);
	ajouteIdentificateur(n->nom, contexte, T_ENTIER, address, NO_COMPLEMENT);

	affiche_element("varDec", n->nom, display_tree);

	if(var_is_global(n->nom)) {
		mips_generate_space(n->nom, INTEGER_SIZE);

	} else if(var_is_local(n->nom)) {
		mips_alloc_word_in_stack();

	}
}

/*-------------------------------------------------------------------------*/
void parcours_tabDec(n_dec *n)
{
	// check array only global variable
	if(contexte != C_VARIABLE_GLOBALE) {
		sprintf(message_error, "%s array must be global variable", n->nom);
		erreur_semantique(message_error);
	}

	int size_byte_tab = n->u.tabDec_.taille * INTEGER_SIZE;
	int address = calculate_address(contexte, size_byte_tab);
	ajouteIdentificateur(n->nom, contexte, T_TABLEAU_ENTIER, address, n->u.tabDec_.taille);

	char texte[100]; // Max. 100 chars nom tab + taille
	sprintf(texte, "%s[%d]", n->nom, n->u.tabDec_.taille);
	affiche_element("tabDec", texte, display_tree);

	mips_generate_space(n->nom, size_byte_tab);
}
/*-------------------------------------------------------------------------*/
