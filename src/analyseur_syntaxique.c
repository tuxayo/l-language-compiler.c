#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "syntabs.h"
#include "util.h"
#include "symboles.h"
#include "analyseur_syntaxique.h"
#include "analyseur_lexical.h"
#include "premiers.h"
#include "suivants.h"
#include "dico.h"
#include "parcours_arbre_abstrait.h"

int token;
bool display_sytax_tree = true;
extern char yytext[YYTEXT_MAX];


/************************************************************************
*************************************************************************/
// functions for definitive implementations
void consume_else_err(int terminal) {
	if(token != terminal) {
		erreur("syntax error");
	}

	afficher_symbole_terminal(token);
	token = yylex();

}


n_prog* programme() {
	afficher_balise_ouvrante(__func__);

	n_l_dec *opt_dec_variable = optDecVariables();
	n_l_dec *liste_dec_fonctions = listeDecFonctions();
	n_prog *programme = cree_n_prog(opt_dec_variable, liste_dec_fonctions);

	afficher_balise_fermante(__func__);

	return programme;
}


n_l_dec* optDecVariables() {
	n_l_dec *opt_dec_variables = NULL;
	n_l_dec *liste_dec_variables = NULL;
	n_dec *tete = NULL;
	n_l_dec *queue = NULL;
	afficher_balise_ouvrante(__func__);



	if (est_premier(_listeDecVariables_, token)) {
		liste_dec_variables = listeDecVariables();
		tete = liste_dec_variables->tete;
		queue = liste_dec_variables->queue;
		consume_else_err(POINT_VIRGULE);

		opt_dec_variables = cree_n_l_dec(tete, queue);

		afficher_balise_fermante(__func__);
		return opt_dec_variables;

	}

	if(est_suivant(_optDecVariables_, token)) {
		afficher_balise_fermante(__func__);
		return NULL;
	}

	erreur("syntax error");return NULL;
}


n_l_dec* listeDecFonctions() {
	n_l_dec *liste_dec_fonction_result = NULL;
	n_dec *declaration_fonction = NULL;
	n_l_dec *liste_dec_fonctions = NULL;

	afficher_balise_ouvrante(__func__);

	if(est_premier(_declarationFonction_, token)) {
		declaration_fonction = declarationFonction();
		liste_dec_fonctions = listeDecFonctions();

		liste_dec_fonction_result = cree_n_l_dec(declaration_fonction, liste_dec_fonctions);

		afficher_balise_fermante(__func__);
		return liste_dec_fonction_result;
	}

	if(est_suivant(_listeDecFonctions_,token)) {
		afficher_balise_fermante(__func__);
		return NULL;

	}

	erreur("syntax error");return NULL;
}


n_dec* declarationFonction() {
	n_dec *declaration_fonction = NULL;
	afficher_balise_ouvrante(__func__);

	char function_name[YYTEXT_MAX];
	strcpy(function_name, yytext);
	consume_else_err(ID_FCT);

	if(est_premier(_listeParam_,token)) {
		n_l_dec *liste_param = listeParam();
		n_l_dec *opt_dec_variables = optDecVariables();
		if(est_premier(_instructionBloc_,token)) {
			n_instr *instruction_bloc = instructionBloc();
			declaration_fonction = cree_n_dec_fonc(function_name, liste_param, opt_dec_variables, instruction_bloc);

			afficher_balise_fermante(__func__);
			return declaration_fonction;

		}
	}
	erreur("syntax error");return NULL;
}


n_l_dec* listeDecVariables() {
	afficher_balise_ouvrante(__func__);
	if(est_premier(_declarationVariable_, token)) {
		n_dec *declaration_variable = declarationVariable();
		n_l_dec *liste_dec_variable_bis = listeDecVariablesBis();

		n_l_dec *liste_dec_variables_result = cree_n_l_dec(declaration_variable, liste_dec_variable_bis);

		afficher_balise_fermante(__func__);
		return liste_dec_variables_result;
	}

	erreur("syntax error");return NULL;
}


n_dec* declarationVariable() {
	n_dec *declaration_variable = NULL;
	afficher_balise_ouvrante(__func__);
	consume_else_err(ENTIER);
	char var_name[YYTEXT_MAX];
	strcpy(var_name, yytext);
	consume_else_err(ID_VAR);
	int opt_taille_tableau = optTailleTableau();
	if(opt_taille_tableau < 0) {
		declaration_variable = cree_n_dec_var(var_name);

	} else {
		declaration_variable = cree_n_dec_tab(var_name, opt_taille_tableau);
	}

	afficher_balise_fermante(__func__);

	return declaration_variable;

}


n_l_dec* listeDecVariablesBis() {
	n_l_dec *liste_dec_variables_result = NULL;
	n_l_dec *liste_dec_variables_bis = NULL;
	n_dec *declaration_variable = NULL;

	afficher_balise_ouvrante(__func__);

	if(token == VIRGULE) {
		afficher_symbole_terminal(token);
		token = yylex();

		if(est_premier(_declarationVariable_, token)) {
			declaration_variable = declarationVariable();
			liste_dec_variables_bis = listeDecVariablesBis();

			liste_dec_variables_result = cree_n_l_dec(declaration_variable, liste_dec_variables_bis);
			afficher_balise_fermante(__func__);
			return liste_dec_variables_result;
		}
	}


	if(est_suivant(_listeDecVariablesBis_, token)) {
		afficher_balise_fermante(__func__);
		return NULL;
	}

	erreur("syntax error");return NULL;

}


int optTailleTableau() {
	afficher_balise_ouvrante(__func__);

	if(token == CROCHET_OUVRANT) {
		afficher_symbole_terminal(token);
		token = yylex();

		int size_of_table = atoi(yytext);
		consume_else_err(NOMBRE);
		consume_else_err(CROCHET_FERMANT);

		afficher_balise_fermante(__func__);
		return size_of_table;

	}

	if(est_suivant(_optTailleTableau_, token)) {
		afficher_balise_fermante(__func__);
		return -1;
	}

	erreur("syntax error");return -1;
}


n_l_dec* listeParam() {
	n_l_dec *liste_param = NULL;
	afficher_balise_ouvrante(__func__);

	consume_else_err(PARENTHESE_OUVRANTE);
	n_l_dec *opt_dec_variables = optListeDecVariables();

	if(opt_dec_variables != NULL) {
		n_dec *tete  = opt_dec_variables->tete;
		n_l_dec *lastParam = opt_dec_variables->queue;

		liste_param = cree_n_l_dec(tete, lastParam);
	}

	consume_else_err(PARENTHESE_FERMANTE);


	afficher_balise_fermante(__func__);
	return liste_param;

}



n_l_dec* optListeDecVariables() {
	n_l_dec *opt_liste_dec_variables = NULL;
	n_l_dec *liste_dec_variables = NULL;
	n_dec *tete = NULL;
	n_l_dec *queue = NULL;
	afficher_balise_ouvrante(__func__);

	if(est_premier(_listeDecVariables_, token)) {
		liste_dec_variables = listeDecVariables();
		tete = liste_dec_variables->tete;
		queue = liste_dec_variables->queue;

		opt_liste_dec_variables = cree_n_l_dec(tete, queue);

		afficher_balise_fermante(__func__);
		return opt_liste_dec_variables;

	}

	if(est_suivant(_optListeDecVariables_, token)) {
//		opt_liste_dec_variables = cree_n_l_dec(tete, queue);

		afficher_balise_fermante(__func__);
		return NULL;
	}

	erreur("syntax error");return NULL;
}



n_instr* instruction() {
	afficher_balise_ouvrante(__func__);
	n_instr *instruction = NULL;

	if(est_premier(_instructionAffect_, token)) {
		n_instr *instruction_affect = instructionAffect();
		n_var *var = instruction_affect->u.affecte_.var;
		n_exp *exp = instruction_affect->u.affecte_.exp;

		instruction = cree_n_instr_affect(var, exp);

	} else if(est_premier(_instructionBloc_, token)) {
		n_instr *instruction_bloc = instructionBloc();
		n_l_instr *liste = instruction_bloc->u.liste;

		instruction = cree_n_instr_bloc(liste);

	} else if(est_premier(_instructionSi_, token)) {
		n_instr *instruction_si = instructionSi();
		n_exp *exp = instruction_si->u.si_.test;
		n_instr *alors = instruction_si->u.si_.alors;
		n_instr *sinon = instruction_si->u.si_.sinon;

		instruction = cree_n_instr_si(exp, alors, sinon);

	} else if(est_premier(_instructionTantque_, token)) {
		n_instr *instruction_tant_que = instructionTantque();
		n_exp *test = instruction_tant_que->u.tantque_.test;
		n_instr *faire = instruction_tant_que->u.tantque_.faire;

		instruction = cree_n_instr_tantque(test, faire);

	} else if(est_premier(_instructionAppel_, token)) {
		n_instr *instruction_appel = instructionAppel();
		n_appel *appel = instruction_appel->u.appel;

		instruction = cree_n_instr_appel(appel);

	} else if(est_premier(_instructionRetour_, token)) {
		n_instr *instruction_retour = instructionRetour();
		n_exp *expression = instruction_retour->u.retour_.expression;
		instruction = cree_n_instr_retour(expression);


	} else if(est_premier(_instructionEcriture_, token)) {
		n_instr *instruction_ecriture = instructionEcriture();
		n_exp *exp = instruction_ecriture->u.ecrire_.expression;

		instruction = cree_n_instr_ecrire(exp);

	} else if(est_premier(_instructionPour_, token)) {
		n_instr *instruction_pour = instructionPour();

		n_instr *init = instruction_pour->u.pour_.init;
		n_exp *test = instruction_pour->u.pour_.test;
		n_instr *incr = instruction_pour->u.pour_.incr;
		n_instr *faire = instruction_pour->u.pour_.faire;

		instruction = cree_n_instr_pour(init, test, incr, faire);

	} else if(est_premier(_instructionVide_, token)) {
		instructionVide();
		instruction = cree_n_instr_vide();

	} else {
		erreur("syntax error");

	} // if - else

	afficher_balise_fermante(__func__);
	return instruction;

}


n_instr* instructionPour() {
	n_instr *instruction = NULL;
	afficher_balise_ouvrante(__func__);

	consume_else_err(POUR);
	if(est_premier(_instructionAffect_, token)) {
		n_instr *init = instructionAffect();

		if(est_premier(_expression_, token)) {
			n_exp *exp = expression();
			consume_else_err(POINT_VIRGULE);

			if(est_premier(_instructionAffect_, token)) {
				n_instr *incr = instructionAffect();
				consume_else_err(FAIRE);

				if(est_premier(_instructionBloc_, token)) {
					n_instr *faire = instructionBloc();

					instruction = cree_n_instr_pour(init, exp, incr, faire);
					afficher_balise_fermante(__func__);
					return instruction;
				}

			}

		}
	}

	erreur("syntax error");return NULL;
} // instructionPour()


n_instr* instructionAffect() {
	n_instr *instruction_affect = NULL;
	afficher_balise_ouvrante(__func__);
	if(est_premier(_var_, token)) {
		n_var *variable = var();
		consume_else_err(EGAL);

		if(est_premier(_expression_, token)) {
			n_exp *exp = expression();
			consume_else_err(POINT_VIRGULE);
			instruction_affect = cree_n_instr_affect(variable, exp);
			afficher_balise_fermante(__func__);
			return instruction_affect;

		}
	}

	erreur("syntax error");return NULL;
}



n_instr* instructionBloc() {
	n_instr *instruction_bloc = NULL;
	n_l_instr *liste = NULL;
	afficher_balise_ouvrante(__func__);

	if(token == ACCOLADE_OUVRANTE) {
		afficher_symbole_terminal(token);
		token = yylex();

		if(est_premier(_listeInstructions_, token) || est_suivant(_listeInstructions_, token)) {
			liste = listeInstructions();
			consume_else_err(ACCOLADE_FERMANTE);

			instruction_bloc = cree_n_instr_bloc(liste);

			afficher_balise_fermante(__func__);
			return instruction_bloc;

		}

	}

	erreur("syntax error");return NULL;
}



n_l_instr* listeInstructions() {
	n_l_instr *liste_instructions = NULL;
	n_instr *tete = NULL;
	n_l_instr *queue = NULL;

	afficher_balise_ouvrante(__func__);

	if(est_premier(_instruction_, token)) {
		tete = instruction();
		queue = listeInstructions();

		liste_instructions = cree_n_l_instr(tete, queue);
		afficher_balise_fermante(__func__);
		return liste_instructions;

	}

	if(est_suivant(_listeInstructions_, token)) {
		afficher_balise_fermante(__func__);
		return NULL;
	}

	erreur("syntax error");return NULL;
}


n_instr* instructionSi() {
	afficher_balise_ouvrante(__func__);
	consume_else_err(SI);

	if(est_premier(_expression_, token)) {
		n_exp *exp = expression();
		consume_else_err(ALORS);
		if(est_premier(_instructionBloc_, token)) {
			n_instr *instruction_bloc = instructionBloc();
			n_instr *instruction_sinon = optSinon();

			n_instr *instruction_si = cree_n_instr_si(exp, instruction_bloc, instruction_sinon);

			afficher_balise_fermante(__func__);
			return instruction_si;
		}
	}

	erreur("syntax error");return NULL;
}



n_instr* optSinon() {
	n_instr *opt_sinon = NULL;
	afficher_balise_ouvrante(__func__);

	if(token == SINON) {
		afficher_symbole_terminal(token);
		token = yylex();

		if(est_premier(_instructionBloc_, token)) {
			opt_sinon = instructionBloc();
			afficher_balise_fermante(__func__);
			return opt_sinon;

		}
	}


	if(est_suivant(_optSinon_, token)) {
		afficher_balise_fermante(__func__);
		return NULL;

	}

	erreur("syntax error");return NULL;
}



n_instr* instructionTantque() {
	n_instr *instruction_tantque = NULL;
	afficher_balise_ouvrante(__func__);
	consume_else_err(TANTQUE);

	if(est_premier(_expression_, token)) {
		n_exp *test = expression();
		consume_else_err(FAIRE);

		if(est_premier(_instructionBloc_, token)) {
			n_instr *faire = instructionBloc();
			instruction_tantque = cree_n_instr_tantque(test, faire);

			afficher_balise_fermante(__func__);
			return instruction_tantque;

		}

	}

	erreur("syntax error");return NULL;

}



n_instr* instructionAppel() {
	n_instr *instruction_appel = NULL;
	afficher_balise_ouvrante(__func__);
	if(est_premier(_appelFct_, token)) {
		n_appel *appel = appelFct();

		consume_else_err(POINT_VIRGULE);
		instruction_appel = cree_n_instr_appel(appel);

		afficher_balise_fermante(__func__);
		return instruction_appel;

	}

	erreur("syntax error");return NULL;
}

n_instr* instructionRetour() {
	n_instr *instruction_retour = NULL;
	afficher_balise_ouvrante(__func__);
	consume_else_err(RETOUR);
	if(est_premier(_expression_, token)) {
		n_exp* exp = expression();
		consume_else_err(POINT_VIRGULE);

		instruction_retour = cree_n_instr_retour(exp);

		afficher_balise_fermante(__func__);
		return instruction_retour;

	}

	erreur("syntax error");return NULL;
}




n_instr* instructionEcriture() {
	n_instr *instruction_ecriture = NULL;
	afficher_balise_ouvrante(__func__);

	consume_else_err(ECRIRE);
	consume_else_err(PARENTHESE_OUVRANTE);
	if(est_premier(_expression_, token)) {
		n_exp *exp = expression();
		consume_else_err(PARENTHESE_FERMANTE);
		consume_else_err(POINT_VIRGULE);
		instruction_ecriture = cree_n_instr_ecrire(exp);

		afficher_balise_fermante(__func__);
		return instruction_ecriture;

	}

	erreur("syntax error");return NULL;
}


n_instr* instructionVide() {
	n_instr *instruction_vide = NULL;
	afficher_balise_ouvrante(__func__);
	consume_else_err(POINT_VIRGULE);
	instruction_vide = cree_n_instr_vide();

	afficher_balise_fermante(__func__);
	return instruction_vide;

}


n_exp* expression() {
	n_exp *expression = NULL;
	n_exp *conj = NULL;
	afficher_balise_ouvrante(__func__);
	if(est_premier(_conjonction_, token)) {
		conj = conjonction();
		expression = expressionBis(conj);

		afficher_balise_fermante(__func__);
		return expression;
	}

	erreur("syntax error");return NULL;
}


n_exp* expressionBis(n_exp* herite) {
	n_exp* herite_fils = NULL;
	n_exp* exp_bis = NULL;
	n_exp* conj = NULL;

	afficher_balise_ouvrante(__func__);
	if(token == OU) {
		afficher_symbole_terminal(token);
		token = yylex();

		if(est_premier(_conjonction_, token)) {
			conj = conjonction();
			herite_fils = cree_n_exp_op(ou, herite, conj);
			exp_bis = expressionBis(herite_fils);

			afficher_balise_fermante(__func__);
			return exp_bis;
		}
	}

	if(est_suivant(_expressionBis_, token)) {

		afficher_balise_fermante(__func__);
		return herite;
	}

	erreur("syntax error");return NULL;
}

n_exp* conjonction() {
	n_exp* conj = NULL;
	n_exp* neg = NULL;
	afficher_balise_ouvrante(__func__);
	if(est_premier(_negation_, token)) {
		neg = negation();
		conj = conjonctionBis(neg);

		afficher_balise_fermante(__func__);
		return conj;

	}

	erreur("syntax error");return NULL;
}


n_exp* conjonctionBis(n_exp* herite) {
	n_exp* neg = NULL;
	n_exp* conjonction_bis = NULL;
	n_exp* herite_fils = NULL;
	afficher_balise_ouvrante(__func__);
	if(token == ET) {
		afficher_symbole_terminal(token);
		token = yylex();
		if(est_premier(_negation_, token)) {
			neg = negation();
			herite_fils = cree_n_exp_op(et, herite, neg);
			conjonction_bis = conjonctionBis(herite_fils);

			afficher_balise_fermante(__func__);
			return conjonction_bis;
		}
	}

	if(est_suivant(_conjonctionBis_, token)) {
		afficher_balise_fermante(__func__);
		return herite;
	}

	erreur("syntax error");return NULL;
}


n_exp* negation() {
	n_exp* neg = NULL;
	n_exp* comp = NULL;
	afficher_balise_ouvrante(__func__);
	if(token == NON) {
		afficher_symbole_terminal(token);
		token = yylex();
		comp = comparaison();
		neg = cree_n_exp_op(non, comp, NULL);

		afficher_balise_fermante(__func__);
		return neg;
	}

	if(est_premier(_comparaison_, token)) {
		neg = comparaison();

		afficher_balise_fermante(__func__);
		return neg;
	}

	erreur("syntax error");return NULL;
}


n_exp* comparaison() {
	n_exp* comparaison = NULL;
	n_exp* exp_arith = NULL;
	afficher_balise_ouvrante(__func__);
	if(est_premier(_expArith_, token)) {
		exp_arith = expArith();
		comparaison = comparaisonBis(exp_arith);
		afficher_balise_fermante(__func__);
		return comparaison;
	}

	erreur("syntax error");return NULL;
}


n_exp* comparaisonBis(n_exp* herite) {
	n_exp* exp_arith = NULL;
	n_exp* comparaison_bis = NULL;
	n_exp* herite_fils = NULL;
	afficher_balise_ouvrante(__func__);

	if(token == EGAL) {
		afficher_symbole_terminal(token);
		token = yylex();
		if(est_premier(_expArith_, token)) {
			exp_arith = expArith();
			herite_fils = cree_n_exp_op(egal, herite, exp_arith);
			comparaison_bis = comparaisonBis(herite_fils);
			afficher_balise_fermante(__func__);
			return comparaison_bis;

		}

	}

	if(token == INFERIEUR) {
		afficher_symbole_terminal(token);
		token = yylex();
		if(est_premier(_expArith_, token)) {
			exp_arith = expArith();
			herite_fils = cree_n_exp_op(inf, herite, exp_arith);
			comparaison_bis = comparaisonBis(herite_fils);
			afficher_balise_fermante(__func__);
			return comparaison_bis;

		}
	}

	if(est_suivant(_comparaisonBis_, token)) {
		afficher_balise_fermante(__func__);
		return herite;

	}

	erreur("syntax error");return NULL;
} // comparaisonBis()



n_exp* expArith() {
	n_exp* exp_arith = NULL;
	n_exp* terme_ = NULL;
	afficher_balise_ouvrante(__func__);

	if(est_premier(_terme_, token)) {
		terme_ = terme();
		exp_arith = expArithBis(terme_);

		afficher_balise_fermante(__func__);
		return exp_arith;

	}

	erreur("syntax error");return NULL;
}


n_exp* expArithBis(n_exp* herite) {
	n_exp* exp_artih_bis = NULL;
	n_exp* terme_ = NULL;
	n_exp* herite_fils = NULL;
	afficher_balise_ouvrante(__func__);

	if(token == PLUS) {
		afficher_symbole_terminal(token);
		token = yylex();
		if(est_premier(_terme_, token)) {
			terme_ = terme();
			herite_fils = cree_n_exp_op(plus, herite, terme_);
			exp_artih_bis = expArithBis(herite_fils);

			afficher_balise_fermante(__func__);
			return exp_artih_bis;
		}

	}

	if(token == MOINS) {
		afficher_symbole_terminal(token);
		token = yylex();
		if(est_premier(_terme_, token)) {
			terme_ = terme();
			herite_fils = cree_n_exp_op(moins, herite, terme_);
			exp_artih_bis = expArithBis(herite_fils);

			afficher_balise_fermante(__func__);
			return exp_artih_bis;
		}

	}

	if(est_suivant(_expArithBis_, token)) {
		exp_artih_bis = herite;
		afficher_balise_fermante(__func__);
		return exp_artih_bis;
	}

	erreur("syntax error");return NULL;
} // expArithBis()


n_exp* terme() {
	n_exp* terme_ = NULL;
	n_exp* facteur_ = NULL;

	afficher_balise_ouvrante(__func__);
	if(est_premier(_facteur_, token)) {
		facteur_ = facteur();
		terme_ = termeBis(facteur_);
		afficher_balise_fermante(__func__);
		return terme_;
	}

	erreur("syntax error");return NULL;
}


n_exp* termeBis(n_exp* herite) {
	n_exp* terme_bis = NULL;
	n_exp* facteur_ = NULL;
	n_exp* herite_fils = NULL;
	afficher_balise_ouvrante(__func__);

	if(token == FOIS) {
		afficher_symbole_terminal(token);
		token = yylex();
		if(est_premier(_facteur_, token)) {
			facteur_ = facteur();
			herite_fils = cree_n_exp_op(fois, herite, facteur_);
			terme_bis = termeBis(herite_fils);

			afficher_balise_fermante(__func__);
			return terme_bis;
		}

	}

	if(token == DIVISE) {
		afficher_symbole_terminal(token);
		token = yylex();
		if(est_premier(_facteur_, token)) {
			facteur_ = facteur();
			herite_fils = cree_n_exp_op(divise, herite, facteur_);
			terme_bis = termeBis(herite_fils);

			afficher_balise_fermante(__func__);

			return terme_bis;
		}

	}

	if(est_suivant(_termeBis_, token)) {
		terme_bis = herite;
		afficher_balise_fermante(__func__);
		return terme_bis;
	}

	erreur("syntax error");return NULL;
}


n_exp* facteur() {
	n_exp* facteur_ = NULL;

	afficher_balise_ouvrante(__func__);
	if(token == PARENTHESE_OUVRANTE) {
		afficher_symbole_terminal(token);
		token = yylex();

		if(est_premier(_expression_, token)) {
			facteur_ = expression();

			consume_else_err(PARENTHESE_FERMANTE);

			afficher_balise_fermante(__func__);
			return facteur_;

		}
	}


	if(token == NOMBRE) {
		char entier_lu[YYTEXT_MAX];
		strcpy(entier_lu, yytext);
		afficher_symbole_terminal(token);
		token = yylex();

		int entier = atoi(entier_lu);
		facteur_ = cree_n_exp_entier(entier);

		afficher_balise_fermante(__func__);
		return facteur_;

	}

	if(est_premier(_appelFct_, token)) {
		n_appel *appel_fct = NULL;
		appel_fct = appelFct();
		facteur_ = cree_n_exp_appel(appel_fct);
		afficher_balise_fermante(__func__);
		return facteur_;

	}

	if(est_premier(_var_, token)) {
		n_var* var_ = var();
		facteur_ = cree_n_exp_var(var_);
		afficher_balise_fermante(__func__);
		return facteur_;

	}

	if(token == LIRE) {
		afficher_symbole_terminal(token);
		token = yylex();
		if(token == PARENTHESE_OUVRANTE) {
			afficher_symbole_terminal(token);
			token = yylex();
			consume_else_err(PARENTHESE_FERMANTE);

			facteur_ = cree_n_exp_lire();

			afficher_balise_fermante(__func__);
			return facteur_;


		}
	}

	erreur("syntax error");return NULL;
} // facteur()


n_var* var() {
	afficher_balise_ouvrante(__func__);
	if(token == ID_VAR) {
		char var_name[YYTEXT_MAX];
		strcpy(var_name, yytext);
		afficher_symbole_terminal(token);
		token = yylex();
		n_exp* indice = optIndice();
		n_var* var = NULL;
		if (indice == NULL) {
			var = cree_n_var_simple(var_name);
		} else {
			var = cree_n_var_indicee(var_name,indice);
		}

		if(est_suivant(_var_, token)) {
			afficher_balise_fermante(__func__);
			return var;
		}

	}

	erreur("syntax error");return NULL;
}


n_exp* optIndice() {
	n_exp* indice = NULL;
	afficher_balise_ouvrante(__func__);
	if(token == CROCHET_OUVRANT) {
		afficher_symbole_terminal(token);
		token = yylex();
		if(est_premier(_expression_, token)) {
			indice = expression();
			if(token == CROCHET_FERMANT) {
				afficher_symbole_terminal(token);
				token = yylex();
				afficher_balise_fermante(__func__);
				return indice;
			}
		}
	}

	if(est_suivant(_optIndice_, token)) {
		afficher_balise_fermante(__func__);
		return NULL;
	}

	erreur("syntax error");return NULL;
}


n_appel* appelFct() {
	n_appel *appel_fonction = NULL;
	n_l_exp* args = NULL;
	afficher_balise_ouvrante(__func__);
	char id_function[YYTEXT_MAX];
	strcpy(id_function, yytext);
	consume_else_err(ID_FCT);
	consume_else_err(PARENTHESE_OUVRANTE);
	args = listeExpressions();
	consume_else_err(PARENTHESE_FERMANTE);
	afficher_balise_fermante(__func__);
	appel_fonction = cree_n_appel(id_function, args);
	return appel_fonction;
}


n_l_exp* listeExpressions() {
	n_l_exp* liste_expressions = NULL;
	afficher_balise_ouvrante(__func__);
	n_exp *exp = NULL;
	n_l_exp *liste_expressions_bis = NULL;
	if(est_premier(_expression_, token)) {
		exp = expression();
		liste_expressions_bis = listeExpressionsBis();

		liste_expressions = cree_n_l_exp(exp, liste_expressions_bis);

		afficher_balise_fermante(__func__);
		return liste_expressions;
	}

	if(est_suivant(_listeExpressions_, token)) {
		afficher_balise_fermante(__func__);
		return NULL;
	}


	return NULL;
}


n_l_exp* listeExpressionsBis() {
	n_l_exp *liste_expressions_bis_result = NULL;
	n_exp *exp = NULL;
	n_l_exp *liste_expressions_bis = NULL;

	afficher_balise_ouvrante(__func__);

	if(token == VIRGULE) {
		afficher_symbole_terminal(token);
		token = yylex();
		if(est_premier(_expression_, token)) {
			exp = expression();
			liste_expressions_bis = listeExpressionsBis();

			liste_expressions_bis_result = cree_n_l_exp(exp, liste_expressions_bis);

			afficher_balise_fermante(__func__);
			return liste_expressions_bis_result;

		}
	}

	if(est_suivant(_listeExpressionsBis_, token)) {
		afficher_balise_fermante(__func__);
		return NULL;
	}

	erreur("syntax error");return NULL;
}


n_prog* test_syntax_parser_internal(bool p_display_sytax_tree) {
	display_sytax_tree = p_display_sytax_tree;
	token = yylex();
	initialise_premiers();
	initialise_suivants();
	n_prog *programme_ = programme();
	if(token != FIN) erreur("Syntax error: expected EOF");
	return programme_;
}
