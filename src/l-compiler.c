/*
 * l-compiler.c
 *
 *  Created on: 24 mars 2015
 *      Author: Didier_TA
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "analyseur_syntaxique.h"
#include "parcours_arbre_abstrait.h"

extern FILE *yyin;

void open_file(char **argv) {
	yyin = fopen(argv[1], "r");
	if(yyin == NULL) {
		fprintf(stderr, "impossible d'ouvrir le fichier %s\n", argv[1]);
		exit(1);
	}
}

int main(int argc, char **argv) {
	open_file(argv);
	n_prog *programme = test_syntax_parser_internal(false);
	parcours_n_prog(programme, mips);
	return 0;
}
