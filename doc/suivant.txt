suivant(programme) = {FIN}

suivant(optDecVariables) = premier(listeDecFonctions)-- + premier(instructionBloc)-- + suivant(programme)
						 = {ID_FCT} + { ACCOLADE_OUVRANTE } + {FIN}
						 = {ID_FCT, ACCOLADE_OUVRANTE, FIN}	




suivant(listeDecVariables) = suivant(optListeDecVariables) + {POINT_VIRGULE}
						   = { PARENTHESE_FERMANTE} + {POINT_VIRGULE }
						   = { PARENTHESE_FERMANTE, POINT_VIRGULE }

suivant(optListeDecVariables) = { PARENTHESE_FERMANTE }

suivant(listeDecVariablesBis) = suivant(listeDecVariables)
							  = { PARENTHESE_FERMANTE, POINT_VIRGULE }

suivant(declarationVariable) = premier(listeDecVariablesBis)-- + suivant(listeDecVariables) + suivant(listeDecVariablesBis) 
							 = {VIRGULE, PARENTHESE_FERMANTE, POINT_VIRGULE }

suivant(optTailleTableau) = suivant(declarationVariable)
						  = {VIRGULE, PARENTHESE_FERMANTE, POINT_VIRGULE }

suivant(listeDecFonctions) = suivant(programme) 
						   = {FIN}

suivant(declarationFonction) = premier(listeDecFonctions)-- + suivant(listeDecFonction)
							 = {ID_FCT, FIN}

suivant(listeParam) = premier(optDecVariables)-- + premier(instructionBloc)
					= {ENTIER, ACCOLADE_OUVRANTE}

suivant(instruction) = premier(listeInstructions)-- + suivant(listeInstructions)
					 = {ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, ID_FCT, RETOUR, ECRIRE, POINT_VIRGULE} + {ACCOLADE_FERMANTE}
					 = {ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, ID_FCT, RETOUR, ECRIRE, POINT_VIRGULE, ACCOLADE_FERMANTE}

suivant(instructionAffect) = suivant(instruction) 
						   = {ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, ID_FCT, RETOUR, ECRIRE, POINT_VIRGULE, ACCOLADE_FERMANTE}

----------------------------------------------------------------------
suivant(instructionBloc) = suivant(declarationFonction) + suivant(instruction) + premier(optSinon)-- + suivant(instructionSi)
						   + suivant(optSinon) + suivant(instructionTantque) + suivant(instructionPour)

						 = {ID_FCT, FIN} + {ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, ID_FCT, RETOUR, ECRIRE, POINT_VIRGULE, ACCOLADE_FERMANTE} + {SINON}
						   + suivant(instructionSi) + suivant(instruction)

						 = {ID_FCT, FIN, ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, RETOUR, ECRIRE, POINT_VIRGULE, ACCOLADE_FERMANTE, SINON}
						   + suivant(instruction) + suivant(instruction)

						 = {ID_FCT, FIN, ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, RETOUR, ECRIRE, POINT_VIRGULE, ACCOLADE_FERMANTE, SINON}


suivant(instructionSi) = suivant(instruction)
					   = {ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, ID_FCT, RETOUR, ECRIRE, POINT_VIRGULE, ACCOLADE_FERMANTE}

suivant(instructionTantque) = suivant(instruction)
							= {ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, ID_FCT, RETOUR, ECRIRE, POINT_VIRGULE, ACCOLADE_FERMANTE}

suivant(instructionPour) = suivant(instruction)
							= {ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, ID_FCT, RETOUR, ECRIRE, POINT_VIRGULE, ACCOLADE_FERMANTE}

suivant(optSinon) = suivant(instructionSi) 
				  = suivant(instruction) 
				  = {ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, ID_FCT, RETOUR, ECRIRE, POINT_VIRGULE, ACCOLADE_FERMANTE}

-----------------------------------------------------------
suivant(listeInstructions) = {ACCOLADE_FERMANTE}

suivant(instructionAppel) = suivant(instruction) 
						  = {ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, ID_FCT, RETOUR, ECRIRE, POINT_VIRGULE, ACCOLADE_FERMANTE}

suivant(instructionRetour) = suivant(instruction) 
						   = {ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, ID_FCT, RETOUR, ECRIRE, POINT_VIRGULE, ACCOLADE_FERMANTE}

suivant(instructionEcriture) = suivant(instruction) 
							 = {ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, ID_FCT, RETOUR, ECRIRE, POINT_VIRGULE, ACCOLADE_FERMANTE}

suivant(instructionVide) = suivant(instruction) 
						 = {ID_VAR, ACCOLADE_OUVRANTE, SI, TANT_QUE, POUR, ID_FCT, RETOUR, ECRIRE, POINT_VIRGULE, ACCOLADE_FERMANTE}

suivant(expression) = {POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT} + premier(listeExpressionsBis)-- + suivant(listeExpressionsBis)
					= {POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}

suivant(expressionBis) = suivant(expression) 
					   = {POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}

suivant(conjonction) = premier(expressionBis)-- + suivant(expression) + suivant(expressionBis)
					 = {OU} + {POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}
					 = {OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}

suivant(conjonctionBis) = suivant(conjonction)
					 	= {OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}

suivant(negation) = premier(conjonctionBis)-- + suivant(conjonction) + suivant(conjonctionBis)
				  = {ET} + {OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}
				  = {ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}



suivant(comparaison) = suivant(negation)
					 = {ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}

suivant(comparaisonBis) = suivant(comparaison) 
						= {ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}

suivant(expArith) = premier(comparaisonBis)-- + suivant(comparaison) + suivant(comparaisonBis)
					= {EGAL, INFERIEUR} + {ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}
					= {EGAL, INFERIEUR, ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}

suivant(expArithBis) = suivant(expArith) 
					 = {EGAL, INFERIEUR, ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}

suivant(terme) = premier(expArithBis)-- + suivant(expArith) + suivant(expArithBis)
			   = {PLUS, MOINS} + {EGAL, INFERIEUR, ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}
			   = {PLUS, MOINS, EGAL, INFERIEUR, ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}


suivant(termeBis) = suivant(terme) 
				  = {PLUS, MOINS, EGAL, INFERIEUR, ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}

suivant(facteur) = premier(termeBis)-- + suivant(terme) + suivant(termeBis)
				 = {FOIS, DIVISER} + {PLUS, MOINS, EGAL, INFERIEUR, ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}
				 = {FOIS, DIVISER, PLUS, MOINS, EGAL, INFERIEUR, ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}


suivant(var) = {EGAL} + suivant(facteur)
			 = {FOIS, DIVISER, PLUS, MOINS, EGAL, INFERIEUR, ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}

suivant(optInd) = suivant(var)
				= {FOIS, DIVISER, PLUS, MOINS, EGAL, INFERIEUR, ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE}

suivant(appelFct) = suivant(facteur) + {POINT_VIRGULE}
				  = {FOIS, DIVISER, PLUS, MOINS, EGAL, INFERIEUR, ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE} + {POINT_VIRGULE}
				  = {FOIS, DIVISER, PLUS, MOINS, EGAL, INFERIEUR, ET, OU, POINT_VIRGULE, ALORS, FAIRE, PARENTHESE_FERMANTE, CROCHET_FERMANT, VIRGULE, POINT_VIRGULE}

suivant(listeExpressions) = { PARENTHESE_FERMANTE }

suivant(listeExpressionsBis) = suivant(listeExpressions)
							 = { PARENTHESE_FERMANTE }



