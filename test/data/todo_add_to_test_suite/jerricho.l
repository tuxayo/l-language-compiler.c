pair(entier $f)
{
	si ($f - ($f / 2) * 2) = 0 alors 
	{
		retour 1;
	}
	sinon 
	{
		retour 0;
	}
}

mult(entier $d, entier $e)
{
	retour $d * $e;
}	
incr(entier $b, entier $c)
{
	retour $b + $c;
}
main()
entier $a;
    {
    $a = lire();
    ecrire($a);
    si pair($a)=1 alors
    {
	    retour mult(
                    incr($a,10),
                    mult(
                        incr($a,2),
                        incr(1,$a)
                        )
                   );
    }
    sinon
    {
	    retour mult($a,2);
    }
}
