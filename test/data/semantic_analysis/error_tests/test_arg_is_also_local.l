max(entier $a, entier $b)
entier $a;
{
	
	$a = 1;
	si $a < $b alors {
		retour $b;
	}

	retour $a;
}

main()
entier $a, entier $v1, entier $v2;
{
	$v1 = 5;
	$v2 = 6;
	$a = max($v1, $v2);
	ecrire($a);
}