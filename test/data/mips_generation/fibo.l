fibonacci(entier $n) {
	si $n < 2 alors {
		retour $n;
	}
	sinon {
		retour (fibonacci($n - 1) + fibonacci($n - 2));
	}
}

main()
entier $a;
{
	$a = lire();
	ecrire(fibonacci($a));
}