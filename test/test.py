#!/usr/bin/env python3
import os
import sys
import subprocess

def main():
    build()
    run_tests()


def build():
    os.chdir("../src") # cd to src for build

    make_result = os.system("make all")
    if (make_result != 0):
        print("######## BUILD FAILED see errors above")
        sys.exit(1)
    os.chdir("../test") # go back to test directory

    print("### BUILD SUCCESS")


def run_tests():

    test_lexical_parser("only_simple_symbols_and_comment")
    test_lexical_parser("tri")
    test_lexical_parser("affect")
    test_lexical_parser("boucle")
    test_lexical_parser("expression")
    test_lexical_parser("max")

    test_syntax_parser("affect")
    test_syntax_parser("boucle")
    test_syntax_parser("expression")
    test_syntax_parser("max")
    test_syntax_parser("tri")
    test_syntax_parser("empty")
    test_syntax_parser("eval1")
    test_syntax_parser("eval2")
    test_syntax_parser("eval3")
    test_syntax_parser("eval4")
    test_syntax_parser("eval5")
    test_syntax_parser("eval6")
    test_syntax_parser("eval7")
    test_syntax_parser("eval8")
    test_syntax_parser("test1")
    test_syntax_parser("test2")
    test_syntax_parser("test3")

    test_error_syntax_parser("double_semi_colon_affect")
    test_error_syntax_parser("superior_symbol")
    test_error_syntax_parser("affect_with_ecrire")
    test_error_syntax_parser("declare_other_type_but_entier")
    test_error_syntax_parser("eval1err")
    test_error_syntax_parser("eval2err")
    test_error_syntax_parser("eval3err")
    test_error_syntax_parser("eval4err")
    test_error_syntax_parser("eval5err")
    test_error_syntax_parser("test1err")

    test_abstract_tree("affect")
    test_abstract_tree("boucle")
    test_abstract_tree("expression")
    test_abstract_tree("max")
    test_abstract_tree("tri")
    test_abstract_tree("empty")

    test_symbol_table("affect")
    test_symbol_table("boucle")
    test_symbol_table("expression")
    test_symbol_table("max")
    test_symbol_table("tri")

    generate_mips("affect", test_data_folder="./data/mips_generation/")
    test_generated_mips("affect")

    generate_mips("max", test_data_folder="./data/mips_generation/")
    test_generated_mips("max", input_sample_number=1)
    test_generated_mips("max", input_sample_number=2)
    test_generated_mips("max", input_sample_number=3)


    generate_mips("boucle", test_data_folder="./data/mips_generation/")
    test_generated_mips("boucle")


    generate_mips("expression", test_data_folder="./data/mips_generation/")
    test_generated_mips("expression", input_sample_number=1)
    test_generated_mips("expression", input_sample_number=2)
    test_generated_mips("expression", input_sample_number=3)
    test_generated_mips("expression", input_sample_number=4)

    generate_mips("tri", test_data_folder="./data/mips_generation/")
    test_generated_mips("tri")

    generate_mips("main_with_local_var_and_return", test_data_folder="./data/mips_generation/")
    test_generated_mips("main_with_local_var_and_return")
    
    generate_mips("fibo", test_data_folder="./data/mips_generation/")
    test_generated_mips("fibo", input_sample_number=1)

    generate_mips("court_circuit", test_data_folder="./data/mips_generation/")
    test_generated_mips("court_circuit")

    print("\n### ALL TESTS PASSED")


def test_lexical_parser(test_name):
    test(test_name=test_name,
        test_data_folder="./data/lexical_parser/",
        binary_to_test="../src/test_yylex",
        result_extension=".lex")


def test_syntax_parser(test_name):
    test(test_name=test_name,
        test_data_folder="./data/syntax_parser/",
        binary_to_test="../src/test_analyse_syntaxique",
        result_extension=".synt")


def test_error_syntax_parser(test_name):
    test(test_name=test_name,
        test_data_folder="./data/syntax_parser/error_tests/",
        binary_to_test="../src/test_analyse_syntaxique",
        result_extension=".synt",
        error_test=True)


def test_abstract_tree(test_name):
    test(test_name=test_name,
        test_data_folder="./data/abstract_tree/",
        binary_to_test="../src/test_abstract_tree",
        result_extension=".asynt")


def test_symbol_table(test_name):
    test(test_name=test_name,
        test_data_folder="./data/symbol_table/",
        binary_to_test="../src/test_symbol_table",
        result_extension=".tab")

def test_generated_mips(test_name, input_sample_number=None):
    test(test_name=test_name,
        test_data_folder="./data/mips_generation/",
        binary_to_test="java -jar mars4_5.jar",
        result_extension=".stdout_exec",
        input_extension=".mips.last_result",
        input_sample_number=input_sample_number)

def generate_mips(test_name, test_data_folder):
    test_command = "../src/l-compiler" + " " + test_data_folder + test_name + ".l"
    output_file = test_data_folder + test_name + ".mips" + ".last_result"
    run_and_redirect_stdout_to_file(test_command, output_file, test_name)

def test(test_name, test_data_folder, binary_to_test, result_extension, input_extension=".l",
        error_test=False, input_sample_number=None):

    if input_sample_number is None:
        optional_stdin_input = ""
    else:
        stdin_input_file =  test_data_folder + test_name + ".stdin_input_" + str(input_sample_number) + ".txt"
        optional_stdin_input = "cat " + stdin_input_file + " | "
        result_extension = result_extension + "_" + str(input_sample_number)

    test_command = optional_stdin_input + binary_to_test + " " + test_data_folder + test_name + input_extension
    output_file = test_data_folder + test_name + result_extension + ".last_result"

    if error_test:
        run_and_redirect_everything_to_file_and_ignore_error(test_command, output_file, test_name)
    else:
        run_and_redirect_stdout_to_file(test_command, output_file, test_name)

    check_output(test_name, test_data_folder, result_extension)
    sys.stdout.write('.')
    sys.stdout.flush()


def check_output(test_name, test_data_folder, result_extension):
    expected = test_data_folder + test_name + result_extension + ".expected"
    actual = test_data_folder + test_name + result_extension + ".last_result"
    try:
        diff_output = subprocess.check_output("diff " + expected + " " + actual, shell=True, universal_newlines=True)

    except subprocess.CalledProcessError as e:
        print("\n######## TEST FAILED: " + test_name)
        print("## expectation not met")
        diff_output = e.output
        print("## diff: \n" + diff_output)
        sys.exit(1)


def run_and_redirect_stdout_to_file(command, output_file, test_name):
    redirected_command = command + " > " + output_file
    # print(command) # for debug
    try:
        # only stderr is displayed as stdout is redirected by the shell
        subprocess.check_output(redirected_command, stderr=subprocess.STDOUT, shell=True, universal_newlines=True)
    except subprocess.CalledProcessError as e:
        print("\n######## TEST FAILED: " + test_name + '\n' +
            command + '\n' +
            ": exited with error, see stderr output below")
        sys.stdout.write(e.output)
        sys.exit(1)


'''Redirect stdout and stderr to file and ignore return code errors from command'''
def run_and_redirect_everything_to_file_and_ignore_error(command, output_file, test_name):
    redirected_command = command + " &> " + output_file
    try:
        subprocess.check_output(redirected_command, shell=True, universal_newlines=True)
    except subprocess.CalledProcessError as e:
        sys.stdout.write('.')
        return

    print("\n######## TEST FAILED: " + test_name + ": should have exited with error")
    sys.exit(1)


if __name__ == '__main__':
    main()
