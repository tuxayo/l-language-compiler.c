l-language-compiler.c
====================================

Compiler for the L language written in C. See /doc for details about L. Project for university.

### Run our tests
    cd test
    ./test.py

### Run the lexical parser
    cd src
    make all
    ./test_yylex my_source_code.l

### Run the semantic parser
	cd src
    make all
    ./test_analyse_syntaxique my_source_code.l

### Run the abstract tree
	cd src
    make all
    ./test_abstract_tree my_source_code.l

## Run the final evaluation test
	cd test/data/eval2
	./testAll.sh